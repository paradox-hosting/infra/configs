# Infra configurations

This repo contains all the configurations needed by all nodes in all modes. They're saved in `/opt/displat/`, and symlink'd as needed. This allows every VM to be deployed with every configuration, making switching modes as easy and recreating symlinks.

CI will zip everything in `./files/` and provide it as a Release artefact. Deployments should pull the latest Release.

## Contributing

PRs should be to the `dev` branch. All contributions should include change log entries to `changelog/latest.md`.

To trigger a release, maintainers will move `changelog/latest.md` to be named as the release date + version (eg `changelog/2021-01-01_1.0.0.md`), copy `changelog_template.md` to `changelog/latest.md`, and merge to the `main` branch.
