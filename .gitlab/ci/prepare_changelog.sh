#!/bin/bash

FILE=$(find files/ -regextype posix-extended -regex '^.*\/[0-9]{4}-[0-9]{2}-[0-9]{2}_v([0-9]{1,2}\.){3}md' -print0 | sort -zn | head -1z)

export RELEASE_DATE=$($FILE | grep -P '\d{4}-\d{2}-\d{2}' -o)
export RELEASE_NAME=$(head -1 $FILE | grep -P '(?<=\#\s)(\w|\s)+' -o)
export RELEASE_DESC=$(tail -n +1 $FILE)

_RELEASE_VERS=$($FILE | grep -P 'v(\d{1,2}\.){3}md' -o)
export RELEASE_VERS=${_RELEASE_VERS%.md}

export RELEASE_FILE="displat_config_${RELEASE_VERS}.zip"
export RELEASE_ADDR="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/displat_config/${RELEASE_VERS}/${RELEASE_FILE}"

touch .env

echo "RELEASE_DATE=${RELEASE_DATE}\n" >> .env
echo "RELEASE_NAME=${RELEASE_NAME}\n" >> .env
echo "RELEASE_DESC=${RELEASE_DESC}\n" >> .env
echo "RELEASE_VERS=${RELEASE_VERS}\n" >> .env
echo "RELEASE_FILE=${RELEASE_FILE}\n" >> .env
echo "RELEASE_ADDR=${RELEASE_ADDR}\n" >> .env
